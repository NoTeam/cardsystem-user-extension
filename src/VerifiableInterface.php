<?php

namespace NoTeam\UserExtension;

interface VerifiableInterface
{
    public function setToken(string $token);

    public function getToken();

    public function setExpired(\DateTimeInterface $expired);
    
    public function getExpired();

    public function getEmailField();

    public function notify($instance);

    public function getVerifiedAt();

    public function setVerifiedAt(\DateTimeInterface $verifiedAt);
}