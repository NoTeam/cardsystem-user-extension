<?php

namespace NoTeam\UserExtension;

use Illuminate\Support\ServiceProvider;

class UserExtensionServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('noteam.user-verification.email', function ($app) {
            return new EmailVerification(
                $app->get('auth')->getProvider()
            );
        });
    }
}