<?php

namespace NoTeam\UserExtension\Tests;

use NoTeam\UserExtension\Tests\Model\User;
use Illuminate\Support\Facades\Notification;
use NoTeam\UserExtension\Notifications\EmailVerifyNotification;

class EmailVerificationTest extends TestCase
{
    public function test_email_verification_registered()
    {
        $emailVerification = $this->app->get('noteam.user-verification.email');
        $this->assertInstanceOf('NoTeam\UserExtension\EmailVerification', $emailVerification);
    }

    public function test_insatance_value_are_not_same()
    {
        $instance1 = $this->app->get('noteam.user-verification.email')
            ->setExpiration(1);

        $instance2 = $this->app->get('noteam.user-verification.email')
            ->setExpiration(2);

        $this->assertNotSame($instance1->getExpiration(), $instance2->getExpiration());
    }

    public function test_can_change_notification_class()
    {
        $instance = $this->app->get('noteam.user-verification.email')
            ->setNotificationClass(Dummy::class);

        $this->assertSame($instance->getNotificationClass(), Dummy::class);
    }

    public function test_can_get_token()
    {
        $emailVerification = $this->app->get('noteam.user-verification.email');
        $this->assertNotNull($emailVerification->createToken());
    }

    public function test_can_get_mailView()
    {
        $mailView = $this->app->get('noteam.user-verification.email')->getMailView();

        $this->assertNotNull($mailView);
        $this->assertSame($mailView, 'email.user.verification');
    }

    public function test_can_change_expiration()
    {
        $expiration = $this->app->get('noteam.user-verification.email')
            ->setExpiration(600)
            ->getExpiration();

        $this->assertNotNull($expiration);
        $this->assertSame($expiration, 600);

    }

    public function test_get_default_notification_class()
    {
        $notificationClass = $this->app->get('noteam.user-verification.email')
            ->getNotificationClass();

        $this->assertNotNull($notificationClass);
        $this->assertSame($notificationClass, 'NoTeam\UserExtension\Notifications\EmailVerifyNotification');

    }

    public function test_can_change_mailview()
    {
        
        $mailView = $this->app->get('noteam.user-verification.email')
            ->setMailView('email.another-email')
            ->getMailView();

        $this->assertNotNull($mailView);
        $this->assertSame($mailView, 'email.another-email');
    }

    public function test_runs_the_migrations()
    {
        Notification::fake();

        $user = User::first();
        $user->sendEmailVerification();

        Notification::assertSentTo(
            [$user], EmailVerifyNotification::class
        );

        $verify = $this->app->get('noteam.user-verification.email');
        $this->assertFalse($verify->isVerified($user));
    }
    
    public function test_verify_user()
    {
        Notification::fake();
        
        $user = User::first();
        $user->sendEmailVerification();
        $instance = $this->app->get('noteam.user-verification.email');
        
        $instance->verify($user);

        $this->assertTrue($instance->isVerified($user));

    }
}